-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2021 at 03:26 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `search_product`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `product` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `type_id`, `product`, `created_at`, `updated_at`) VALUES
(1, 1, 'White Tee', '2021-03-17 23:58:10', '2021-03-17 23:58:10'),
(2, 1, 'Printed Tee', '2021-03-17 23:58:39', '2021-03-17 23:58:39'),
(4, 3, 'Retro Glasses', '2021-03-17 23:59:23', '2021-03-17 23:59:23'),
(8, 4, 'Signature Wallet', '2021-03-18 00:01:26', '2021-03-18 00:01:26'),
(10, 2, 'Washed Jeans', '2021-03-18 17:35:53', '2021-03-18 17:35:53'),
(11, 1, 'Black T-shirt', '2021-03-18 17:36:32', '2021-03-18 17:36:32'),
(12, 2, 'Black Jeans', '2021-03-18 17:36:44', '2021-03-18 17:36:44'),
(13, 2, 'Blue Jeans', '2021-03-18 17:37:07', '2021-03-18 17:37:07'),
(14, 3, 'Bamboo Sunglasses', '2021-03-18 17:40:40', '2021-03-18 17:40:40'),
(15, 1, 'Logo Tee', '2021-03-18 17:40:59', '2021-03-18 17:40:59'),
(16, 1, 'Oversized tee', '2021-03-18 17:41:15', '2021-03-18 17:41:15'),
(17, 3, 'Recycled Frame Glasses', '2021-03-18 17:55:51', '2021-03-18 17:55:51');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE `product_tag` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_tag`
--

INSERT INTO `product_tag` (`id`, `product_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 2, 5),
(6, 4, 4),
(7, 4, 3),
(8, 8, 8),
(9, 8, 3),
(10, 10, 6),
(11, 10, 7),
(12, 10, 3),
(13, 11, 1),
(14, 11, 2),
(15, 11, 3),
(16, 11, 7),
(17, 12, 6),
(18, 12, 7),
(19, 12, 3),
(20, 13, 5),
(21, 13, 1),
(22, 13, 6),
(23, 13, 3),
(24, 14, 2),
(25, 15, 2),
(26, 15, 6),
(27, 15, 9),
(28, 16, 1),
(29, 16, 2),
(30, 16, 3),
(31, 17, 8),
(34, 17, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`) VALUES
(1, 'Organic'),
(2, 'Fair trade'),
(3, 'Living Wage'),
(4, 'Zero Waste'),
(5, 'Vegan'),
(6, 'Sustainable Dyes'),
(7, 'Water Treatment'),
(8, 'Recycled'),
(9, 'Charitable donation');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 't-shirt'),
(2, 'jeans'),
(3, 'sunglasses'),
(4, 'wallet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `products` ADD FULLTEXT KEY `search` (`product`);

--
-- Indexes for table `product_tag`
--
ALTER TABLE `product_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `tags` ADD FULLTEXT KEY `search` (`tag`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `type` ADD FULLTEXT KEY `search` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `product_tag`
--
ALTER TABLE `product_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
