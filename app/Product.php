<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $primarykey = 'id';

    public function product() {
		return $this->belongsTo('App\Type','id' ,'type_id');
	}
}
