<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Type;
use View;
use DB;

class SearchController extends Controller
{

    public function search(Request $request){


        if(!$request->search){

            $search = "Search Products";
            $products = DB::select("
                                select product_tag.product_id , product ,type.type , group_concat(tags.tag) as tags
                                from products
                                left join type on products.type_id = type.id
                                left join product_tag on products.id = product_tag.product_id
                                left join tags on product_tag.tag_id = tags.id
                                group by product_tag.product_id 
                                ");

        }else{

            $search = $request->search;
    
            $products = DB:: select("
                                    SELECT 
                                    product_tag.product_id , products.product, type.type, GROUP_CONCAT(tags.tag) as tags ,
                                    MATCH(product) AGAINST ('$search') as psocre ,
                                    MATCH(tags.tag) AGAINST ('$search') as tscore ,
                                    MATCH(type.type) AGAINST ('$search') as typescore 
                                    FROM `products` 
                                    LEFT JOIN product_tag on products.id = product_tag.product_id 
                                    LEFT JOIN tags on product_tag.tag_id = tags.id 
                                    LEFT JOIN type on products.type_id = type.id
                                    WHERE MATCH(product) AGAINST ('$search')
                                    OR MATCH(tags.tag) AGAINST ('$search') 
                                    OR MATCH(type.type) AGAINST ('$search') 
                                    GROUP BY product_tag.product_id
                                    ORDER BY (psocre + tscore + typescore) DESC
            ");
        }



        // dd($search_product);
        if(!empty($products)){

            return View::make('search_product.list')->with(['products'=>$products ,'search_text' => $search]);
        }

        return View::make('search_product.list')->with(['products'=>$products ,'search_text' => $search]);


    }
}
