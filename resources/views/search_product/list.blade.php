@extends('welcome')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center"><b>Product Search</b></h3>
                    <!-- @if (Session::get('error'))
                        
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <b>{{Session::get('error')}}</b> <strong class="font-weight-bold">'{{Session::get('test')}}'</strong>
                        </div>

                    @endif -->
                    <div class="card m-4">
                        <div class="card-header bg-primary">
                            <div class="search float-right"> 
                            <form method="GET" action="{{route('search')}}"> 
                                @csrf
                                    <input type="text" class="pl-2"
                                        placeholder=" {{$search_text}}"
                                        name="search"> 
                                    <button type="submit"> 
                                        <i class="fa fa-search"
                                            style="font-size: 18px;"> 
                                        </i> 
                                    </button> 
                                </form> 
                            </div> 
                        </div>
                        <div class="card-body ">
                            <table datatable class="table table-bordered">
                                <thead>                  
                                    <tr>
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Type</th>
                                        <th>Tag</th>
                                      </tr>
                                </thead>
                                <tbody>
                                   @if(count($products) > 0)
                                    @foreach($products as $product )
                                    <tr>
                                        <td>{{($loop->index)+1}}</td>
                                        <td>{{$product->product}}</td>
                                        <td>{{$product->type}}</td>
                                        <td>{{$product->tags}}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <td colspan="4">No result found</td>
                                    @endif
                                </tbody>
                            </table>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
@endsection
